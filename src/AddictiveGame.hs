{-# LANGUAGE TupleSections #-}
module AddictiveGame where

import AddictiveGame.Game
import Data.List.Extra (groupSort)
import Data.Tuple.Extra (first)
import qualified Data.Set as S
import qualified Data.Map as M
import Debug.Trace
import Control.Monad.State

coordinatesToString :: [Coordinate] -> String
coordinatesToString = unwords . fmap show . untuple
                        where untuple [] = []
                              untuple ((x, y) : ps) = x : y : untuple ps


level1 :: FilePath -> IO ()
level1 f = do (b, _) <- parseBoard' . words <$> readFile f
              let ecs = (traverse positionToCoordinate . M.keys
                          =<< gamePointMap) `runGame` b
              case ecs of
                Left e   -> print e
                Right cs -> putStrLn $ coordinatesToString cs

separateColors :: [Position] -> Game GameError [[Position]]
separateColors ps = do cps <- traverse colorTagPosition ps
                       pure . fmap snd . groupSort $ cps
                     where colorTagPosition p = (, p) <$> positionColor p

level2 :: FilePath -> IO ()
level2 f = do (b, _) <- parseBoard . words <$> readFile f
          {- separateColors ps :: Game GameError [[Position]]              -}
          {- traverse findDistance :: [[Position]] -> Game GameError [Int] -}
              let er = (traverse findDistance
                          =<< separateColors . M.keys
                          =<< gamePointMap) `runGame` b
              case er of
                Left e  -> print e
                Right r -> putStrLn $ unwords . fmap show $ r

level3 :: FilePath -> IO ()
level3 f = do (b, ss) <- parseBoard . words <$> readFile f
              let (p, []) = first head $ parsePaths ss
                  ei = runPath p `runGame` b
              case ei of
                Left  i -> putStrLn $ "-1 " <> show i
                Right i -> putStrLn $  "1 " <> show i

runPath :: Path -> Game Int Int
runPath (Path c i ss)
  = do (p, ps) <- traverse f ss `execStateT` (i, S.empty)
       let l = S.size ps
       cp      <- mapGameError (const l) $ positionColor p
       if cp /= c
         then gameThrow l
         else pure l
         where f :: Step
                    -> StateT (Position, S.Set Position) (Game Int) ()
               f s = do (p, ps) <- get
                        let l = S.size ps + 1
                        p' <- lift $ do p' <- mapGameError (const l)
                                                           $ step s p
                                        mc <- safePositionColor p'
                                        if p' `S.member` ps
                                          then gameThrow l
                                          else case mc of
                                                 Nothing -> pure p'
                                                 Just c' -> if c' == c
                                                             then pure p'
                                                             else gameThrow l
                        put (p', S.insert p ps)

level4parser :: [String] -> (Board, [Path])
level4parser = level4parseBoard

tracePath :: Path -> Game GameError [Position]
tracePath = undefined

traceValidPath :: Path -> Game GameError (Maybe [Position])
traceValidPath = undefined

traceValidPaths :: [Path] -> Game GameError [[Position]]
traceValidPaths = undefined

-- addToPos :: Ord a => [[a]] -> State (S.Set a) ()
-- addToPos [] = pure ()
-- addToPos (a:as) = do s <- get
                     -- when (S.disjoint (S.fromList a) s) $ put (S.union (S.fromList a) s)
                     -- addToPos as

-- mergeNonIntersecting :: Ord a => [[a]] -> S.Set a
-- mergeNonIntersecting i = snd $ runState (addToPos i) S.empty

mergeNonIntersecting :: Ord a => [[a]] -> S.Set a
mergeNonIntersecting = foldl f S.empty
                 where f s as = let as' = S.fromList as
                                in if as' `S.disjoint` s
                                     then S.union as' s
                                     else s

traceNonIntersecting :: [Path] -> Game GameError (S.Set Position)
traceNonIntersecting = undefined

renderPositions :: S.Set Coordinate -> String
renderPositions = undefined

--level4 :: FilePath -> IO ()
-- level4 f = do (b,ss) <- parseBoard . words <$> readFile f
--              let (ps,rest) = parsePaths ss
--                  pss = runPath <$> ps
--                  case pss of
--                        (Game i _)  ->  putStrLn $ show i
--                      _ -> putStrLn "error"

