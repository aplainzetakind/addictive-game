{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TupleSections #-}

module AddictiveGame.Game ( Game (..)
                          , runGame
                          , gameThrow
                          , gameCatch
                          , gamePointMap
                          , mapGameError
                          , GameError (..)
                          , Board (..)
                          , parseBoard'
                          , parseBoard
                          , level4parseBoard
                          , Position (..)
                          , Coordinate (..)
                          , Step (..)
                          , step
                          , Path (..)
                          , parsePaths
                          , positionToCoordinate
                          , positionColor
                          , safePositionColor
                          , findDistance
                          ) where

import qualified Data.Map as M
import Control.Monad.Reader
import Control.Monad.Except

data Step = N | E | S | W deriving (Show, Read)
newtype Position = Position Int deriving (Eq, Ord, Show)
newtype Color = Color Int deriving (Eq, Ord, Show)

type Coordinate = (Int, Int)

data Board = Board { getDimensions :: Dimensions
                   , getPointMap   :: PointMap }

data Dimensions = Dimensions { getHeight :: Int
                             , getWidth  :: Int }

type PointMap = M.Map Position Color

parseBoard' :: [String] -> (Board, [String])
parseBoard' (h : w : n : rest)
     = let l = read n
           (ps, rest') = splitAt l rest
       in if length ps == l -- ^ check that there are enough positions
            then (Board (Dimensions (read h) (read w))
                        (M.fromList
                            $ (,Color 0) . Position . read <$> ps), rest')
            else error "parseBoard': Invalid input."
parseBoard' _  = error "parseBoard': Invalid input."

level4parseBoard :: [String] -> (Board, [Path])
level4parseBoard (h : w : n : rest)
   = let l = 2 * read n
         (ps, rest') = splitAt l rest
     in if length ps == 1
           then let b = Board (Dimensions (read h) (read w)) $ f (read <$> ps)
                in (b, fst $ parsePaths rest')
           else error "Parseboard level4 invalid input"
             where f [] = M.empty
                   f (p : c : pcs) = M.insert (Position p) (Color c) $ f pcs
                   f _ = error "Parseboard level4 Odd length list"
level4parseBoard _ = error "parseBoard invalid input"

parseBoard :: [String] -> (Board, [String])
parseBoard (h : w : n : rest)
   = let l = 2 * read n
         (ps, rest') = splitAt l rest
     in if length ps == l
          then let b = Board (Dimensions (read h) (read w)) $ f (read <$> ps)
               in (b, rest')
          else error "parseBoard: Invalid input."
            where f [] = M.empty
                  f (p : c : pcs) = M.insert (Position p) (Color c) $ f pcs
                  f _ = error "parseBoard: Odd length list."
parseBoard _ = error "parseBoard': Invalid input."

data Path = Path { pathColor :: Color
                 , pathInit  :: Position
                 , pathSteps :: [Step] }

parsePath :: [String] -> (Path, [String])
parsePath (c : s : n : rest)
   = let l = read n
         (ss, rest') = splitAt l rest
     in if length ss == l
           then let p = Path (Color $ read c) (Position $ read s)
                          $ read <$> ss
                in (p, rest')
           else error "makePath failed"
parsePath _ = error "makePath failed"

parsePaths :: [String] -> ([Path], [String])
parsePaths (n : ss) = (\(f, s) -> (f [], s)) $ go (read n) ss id
                   where go 0 qs f = (f, qs)
                         go n qs f = let (p, qs') = parsePath qs
                                         in go (n - 1) qs' (f . (p :))

newtype Game e a = Game { unGame :: ExceptT e (Reader Board) a }
                   deriving (Functor, Applicative, Monad)

runGame :: Game e a -> Board -> Either e a
runGame g b = runExceptT (unGame g) `runReader` b

data GameError = InvalidDestination String -- ^ Path failed to arrive at
                                           --   a point of same color.
               | OutOfBounds String -- ^ A coordinate is out of bounds.
               | SelfIntersection String
               | ColorMismatch String -- ^ A path touched a wrong color.
               | PositionOverflow String -- ^ Position does not fit
                                         --   dimensions.
               | NonPairColor String -- ^ Number of points with a given color
                                     --   is not 2.
               | PositionNotFound String -- ^ Looked up a position that's not
                                         --   in the PointMap.
               | Impossible String -- ^ Things that shouldn't happen; bugs.
                   deriving Show

gameDimensions :: Game e Dimensions
gameDimensions = Game $ asks getDimensions

gamePointMap :: Game e PointMap
gamePointMap = Game $ asks getPointMap

gameThrow :: e -> Game e a
gameThrow e = Game $ throwError e

gameCatch :: Game e a -> (e -> Game e a) -> Game e a
gameCatch a f = Game $ unGame a `catchError` (unGame . f)

mapGameError :: (e -> e') -> Game e a -> Game e' a
mapGameError f = Game . withExceptT f . unGame

position :: Int -> Game GameError Position
position n = do Dimensions h w <- gameDimensions
                if n > h * w
                  then gameThrow $ PositionOverflow
                                 $ "called position with "
                                 <> "position " <> show n
                                 <> " on board dimensions " <> show (w, h)
                  else pure $ Position n

coordinate :: Coordinate -> Game GameError Coordinate
coordinate (x, y) = do Dimensions h w <- gameDimensions
                       if x > w || y > h
                         then gameThrow
                                   $ OutOfBounds $ "called coordinate with "
                                                <> show (x, y)
                                                <> " on a board "
                                                <> show (w, h)
                         else pure (x, y)

positionToCoordinate :: Position -> Game GameError Coordinate
positionToCoordinate (Position p)
    = do Dimensions h w <- gameDimensions
         position p -- ^ We don't need the result: it will be p unless this
                    --   throws an exception.
         let (d, r) = p `divMod` w
             c      = if r == 0 then coordinate (w, d)
                                else coordinate (r, d + 1)
         c `gameCatch`
              const (gameThrow $ Impossible
                               $ "Non-overflowing position "
                               <> "resolved to out-of-bounds "
                               <> "coordinate")

coordinateToPosition :: Coordinate -> Game GameError Position
coordinateToPosition (x, y) = do Dimensions _ w <- gameDimensions
                                 coordinate (x, y)
                                 position ((y - 1) * w + x)
                                   `gameCatch`
                                       const (gameThrow
                                              $ Impossible
                                              $ "In-bounds coordinate "
                                              <> "resolved to overflowing "
                                              <> "position")

manhattanDistance :: Position -> Position -> Game GameError Int
manhattanDistance p p'
    = do (x, y)   <- positionToCoordinate p
         (x', y') <- positionToCoordinate p'
         pure $ abs (x - x') + abs (y - y')

findDistance :: [Position] -> Game GameError Int
findDistance [x, y] = manhattanDistance x y
findDistance xs = gameThrow $ NonPairColor $ show xs

safePositionColor :: Position -> Game e (Maybe Color)
safePositionColor p = M.lookup p <$> gamePointMap

positionColor :: Position -> Game GameError Color
positionColor p = do mc <- safePositionColor p
                     case mc of
                       Just c -> pure c
                       Nothing -> gameThrow $ PositionNotFound $ show p

step :: Step -> Position -> Game GameError Position
step s p = do (x, y) <- positionToCoordinate p
              coordinateToPosition $ case s of
                                        N -> (x, y - 1)
                                        E -> (x + 1, y)
                                        S -> (x, y + 1)
                                        W -> (x - 1, y)
